function valido(x,matrix)
	tam = size(matrix,1)
	if x < 1 || x > tam
		return false
	end
	return true
end

function busca_quadrado(tamanho,matrix,x,y)
	soma = 0
	for i in x:x+tamanho-1
		for j in y:y+tamanho-1
			soma += matrix[i,j]
		end
	end	
	return soma
end

function max_sum_submatrix(matrix)
	tam = size(matrix,1)
	tamanho = 1
	maximo = -10000000000000000000
	recorde = []
	for i in 1:tam
		for j in 1:tam
			while valido(i+tamanho-1,matrix) && valido(j+tamanho-1,matrix)
				soma = busca_quadrado(tamanho,matrix,i,j)
				if soma > maximo
					maximo = soma
					recorde = []
					push!(recorde,matrix[i:i+tamanho-1,j:j+tamanho-1])
				elseif soma == maximo
					push!(recorde,matrix[i:i+tamanho-1,j:j+tamanho-1])
				end
				tamanho += 1
			end
			tamanho = 1
		end
	end
	return recorde
end

using Test

function testa()
	@test max_sum_submatrix([1 1 1; 1 1 1; 1 1 1]) == [[1 1 1; 1 1 1; 1 1 1]]
	@test max_sum_submatrix([0 -1; -2 -3]) == [zeros(Int64,1,1)]
	@test max_sum_submatrix([-1 1 1; 1 -1 1; 1 1 -2]) ==[[-1 1 1; 1 -1 1; 1 1 -2],[1 1; -1 1],[1 -1; 1 1]]
	@test max_sum_submatrix([0 -200 0 0;-234 0 -500 0;-55 0 72 56;0 -1000 2 234;]) == [[72 56; 2 234]]
	println("Final dos testes")
end
# testa()